package com.parship.roperty.migration;

import com.parship.roperty.DomainSpecificValue;
import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.KeyValues;
import com.parship.roperty.KeyValuesFactory;
import com.parship.roperty.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isBlank;

@SpringBootApplication
public class RopertyMigrationApp {

    private static final Logger LOGGER = LoggerFactory.getLogger(RopertyMigrationApp.class);

    @Autowired
    private KeyValuesFactory keyValuesFactory;

    @Autowired
    private DomainSpecificValueFactory domainSpecificValueFactory;

    @Autowired
    private Persistence sourcePersistence;

    @Autowired
    private Persistence targetPersistence;

    private int keyCount = 1;

    public static void main(String[] args) {
        SpringApplication.run(RopertyMigrationApp.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext context) {
        return args -> {

            List<String> allKeys = sourcePersistence.getAllKeys();
            final int sourceKeyCount = allKeys.size();

            if (!targetPersistence.getAllKeys().isEmpty()) {
                LOGGER.warn("The target persistence isn't empty. This could lead to duplicate key problems.");
            }

            allKeys.forEach(
                key -> {

                    float progress = 100f * keyCount / sourceKeyCount;
                    if (progress % 1 == 0) {
                        LOGGER.info("Migrating key {} of {} ({} %)", keyCount, sourceKeyCount, progress);
                    }

                    KeyValues keyValues = sourcePersistence.load(key, keyValuesFactory, domainSpecificValueFactory);
                    Set<DomainSpecificValue> domainSpecificValues = keyValues.getDomainSpecificValues();

                    Map<String, KeyValues> valuesByChangeset = new HashMap<>();
                    for (DomainSpecificValue domainSpecificValue : domainSpecificValues) {
                        String changeSet = domainSpecificValue.getChangeSet();
                        KeyValues values = valuesByChangeset.computeIfAbsent(changeSet, (a) -> keyValuesFactory.create(domainSpecificValueFactory));
                        values.setDescription(keyValues.getDescription());
                        String patternStr = domainSpecificValue.getPatternStr();
                        if (isBlank(patternStr)) {
                            values.putWithChangeSet(changeSet, domainSpecificValue.getValue());
                        } else {
                            String[] keyParts = patternStr.split("\\|");
                            values.putWithChangeSet(changeSet, domainSpecificValue.getValue(), keyParts);
                        }
                    }

                    valuesByChangeset.forEach(
                        (changeSet, values) -> targetPersistence.store(key, values, changeSet));

                    keyCount++;
                }
            );

            int newTargetKeyCount = targetPersistence.getAllKeys().size();
            if (newTargetKeyCount == sourceKeyCount) {
                LOGGER.info("Target persistence has same amount of keys than source persistence.");
            } else {
                LOGGER.warn("The source persistence contained {} keys, whereas the target persistence contains {} keys.", sourceKeyCount, newTargetKeyCount);
            }

            LOGGER.info("Migrated {} keys", keyCount);
            SpringApplication.exit(context);
        };
    }


}
