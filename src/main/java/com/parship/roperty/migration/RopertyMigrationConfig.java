package com.parship.roperty.migration;

import com.mongodb.MongoClient;
import com.parship.roperty.DefaultKeyValuesFactory;
import com.parship.roperty.DomainSpecificValueFactory;
import com.parship.roperty.DomainSpecificValueFactoryWithStringInterning;
import com.parship.roperty.KeyValuesFactory;
import com.parship.roperty.Persistence;
import com.parship.roperty.mongodb.CollectionProvider;
import com.parship.roperty.mongodb.MongoDbPersistenceFactory;
import com.parship.roperty.persistence.jpa.LazyJpaPersistence;
import com.parship.roperty.persistence.jpa.QueryBuilder;
import com.parship.roperty.persistence.jpa.QueryBuilderDelegate;
import com.parship.roperty.persistence.jpa.RopertyKey;
import com.parship.roperty.persistence.jpa.RopertyKeyDAO;
import com.parship.roperty.persistence.jpa.RopertyValue;
import com.parship.roperty.persistence.jpa.RopertyValueDAO;
import com.parship.roperty.persistence.jpa.TransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManagerFactory;

/**
 * Created by daniel on 30.03.17.
 */
@Configuration
public class RopertyMigrationConfig {

    @Bean
    public KeyValuesFactory keyValuesFactory() {
        return new DefaultKeyValuesFactory();
    }

    @Bean
    public DomainSpecificValueFactory domainSpecificValueFactory() {
        return new DomainSpecificValueFactoryWithStringInterning();
    }

    @Bean
    public Persistence sourcePersistence() {
        LazyJpaPersistence sourcePersistence = new LazyJpaPersistence();
        sourcePersistence.setTransactionManager(transactionManager());
        sourcePersistence.setRopertyKeyDAO(ropertyKeyDAO());
        sourcePersistence.setRopertyValueDAO(ropertyValueDAO());
        return sourcePersistence;
    }

    @Bean
    public Persistence targetPersistence() {
        MongoDbPersistenceFactory mongoDbPersistenceFactory = new MongoDbPersistenceFactory();
        return mongoDbPersistenceFactory.createLazyPersistence(collectionProvider());
    }

    @Bean
    CollectionProvider collectionProvider() {
        CollectionProvider collectionProvider = new CollectionProvider();
        collectionProvider.setMongo(new MongoClient());
        collectionProvider.setDatabaseName("roperty");
        collectionProvider.setCollectionName("properties");
        return collectionProvider;
    }

    @Bean
    RopertyValueDAO ropertyValueDAO() {
        RopertyValueDAO ropertyValueDAO = new RopertyValueDAO();
        ropertyValueDAO.setQueryBuilderDelegate(valueQueryBuilderDelegate());
        return ropertyValueDAO;
    }

    @Bean
    QueryBuilderDelegate<RopertyValue> valueQueryBuilderDelegate() {
        QueryBuilderDelegate<RopertyValue> valueQueryBuilderDelegate = new QueryBuilderDelegate<>();
        valueQueryBuilderDelegate.setEntityManagerFactory(entityManagerFactory());
        valueQueryBuilderDelegate.setQueryBuilder(new QueryBuilder<>());
        valueQueryBuilderDelegate.setResultClass(RopertyValue.class);
        return valueQueryBuilderDelegate;
    }

    @Bean
    RopertyKeyDAO ropertyKeyDAO() {
        RopertyKeyDAO ropertyKeyDAO = new RopertyKeyDAO();
        ropertyKeyDAO.setQueryBuilderDelegate(keyQueryBuilderDelegate());
        return ropertyKeyDAO;
    }

    @Bean
    QueryBuilderDelegate<RopertyKey> keyQueryBuilderDelegate() {
        QueryBuilderDelegate<RopertyKey> keyQueryBuilderDelegate = new QueryBuilderDelegate<>();
        keyQueryBuilderDelegate.setEntityManagerFactory(entityManagerFactory());
        keyQueryBuilderDelegate.setQueryBuilder(new QueryBuilder<>());
        keyQueryBuilderDelegate.setResultClass(RopertyKey.class);
        return keyQueryBuilderDelegate;
    }

    @Bean
    EntityManagerFactory entityManagerFactory() {
        return javax.persistence.Persistence.createEntityManagerFactory("postgresql");
    }

    @Bean
    TransactionManager transactionManager() {
        TransactionManager transactionManager = new TransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory());
        return transactionManager;
    }

}
