# Roperty Migration

A migration utility to transfer Roperty data from one persistence to another. 

## Building

    mvn clean package
    
## Running

Warning: The relations within the database configured in persistence.xml (see below) will be dropped.

    java -jar target/*.jar
    
## Configuration

### persistence.xml

Location: src/main/resources/META-INF

Contains the JPA connection information.
